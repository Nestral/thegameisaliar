﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class PickUpable : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            PickUp();
        }
    }

    internal abstract void PickUp();
}
