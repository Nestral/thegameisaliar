﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StickyPlatform : MonoBehaviour
{
    public float strength;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            AudioManager.instance.Play("HitSticky");
        }
    }

    private void OnCollisionStay2D(Collision2D other)
    {
        if (other.collider.CompareTag("Player"))
        {
            Vector2 point = other.GetContact(0).point;
            foreach (ContactPoint2D contactPoint2D in other.contacts)
            {                
                point = (point + contactPoint2D.point) / 2;
            }
            other.collider.attachedRigidbody.AddForce((point - (Vector2)other.transform.position).normalized * strength);
        }
    }
}
