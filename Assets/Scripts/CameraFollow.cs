﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{    
    [HideInInspector]
    public Transform FollowCamera { get { return Camera.main.transform; } }
    [Range(0, 1)]
    public float followSmoothness;
    public float speedMultiplier;
    public float mouseMultiplier;
    public float mouseDeadzone;
    public float maxDistance;

    public int minY;
    public int maxY;

    public Vector3 cameraOffset;

    private void OnValidate()
    {
        SnapCamera();
    }

    //Put camera in position based on the given offset
    private void SnapCamera()
    {
        if (FollowCamera != null)
            FollowCamera.position = transform.position + cameraOffset;
    }

    void Update()
    {
        Follow();
    }

    //Change camera's position
    private void Follow()
    {
        Vector3 currentPos = FollowCamera.position;
        Vector3 mouseOffset 
            = (Input.mousePosition - new Vector3(Camera.main.pixelWidth / 2, Camera.main.pixelHeight / 2)) 
            * mouseMultiplier * 0.01f;
        if (mouseOffset.magnitude <= mouseDeadzone)
            mouseOffset = Vector3.zero;
        Vector3 targetPos = transform.position + cameraOffset + mouseOffset;

        //Vector3 offset = rb.velocity * speedMultiplier;
        //if (offset.magnitude > maxDistance)
        //    offset = offset.normalized * maxDistance;

        Vector3 smoothPosition = Vector3.Lerp(currentPos, targetPos, followSmoothness);
        smoothPosition.y = Mathf.Clamp(smoothPosition.y, minY, maxY);

        FollowCamera.position = smoothPosition;
    }
}
