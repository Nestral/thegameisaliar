﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Level : MonoBehaviour
{
    public Transform player;
    private PlayerController playerController;
    private Rigidbody2D playerRB;

    private Vector2 spawnpoint;

    public int minY;
    
    public GameObject pauseMenu;
    public GameObject pausePanel;
    [HideInInspector]
    public bool isPaused;

    public GameObject finishMenu;
    public GameObject prevLevelButton;
    public GameObject nextLevelButton;
    public Text highscoreText;
    public Text currentScoreText;

    public Text currentScore;
    private float timeScore;

    private bool isStarted;
    private bool isFinished;

    private string highscoreKey;
    private string levelUnlockKey = "LevelUnlocked";

    private void Awake()
    {
        highscoreKey = "Highscore" + SceneManager.GetActiveScene().buildIndex;
        spawnpoint = player.position;

        playerRB = player.GetComponent<Rigidbody2D>();
        playerController = player.GetComponent<PlayerController>();
        playerController.currentLevel = this;

        StartLevel();
    }

    public void Reset()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    private void StartLevel()
    {       
        //Reset player
        playerRB.velocity = Vector2.zero;
        playerRB.angularVelocity = 0;
        player.SetPositionAndRotation(spawnpoint, Quaternion.identity);

        //Reset camera
        CameraFollow cameraFollow = player.GetComponent<CameraFollow>();
        cameraFollow.FollowCamera.position = cameraFollow.transform.position + cameraFollow.cameraOffset;

        Time.timeScale = 0f;
        finishMenu.SetActive(false);
        timeScore = 0;
        currentScore.text = "0";
        isFinished = false;
        isStarted = false;
    }

    public void ChangePauseState()
    {
        if (isPaused)
            ResumeGame();
        else PauseGame();
    }

    public void PauseGame()
    {
        isPaused = true;
        pausePanel.SetActive(true);
        pauseMenu.SetActive(true);
        Time.timeScale = 0f;
    }

    public void ResumeGame()
    {
        isPaused = false;
        pausePanel.SetActive(false);
        pauseMenu.SetActive(false);
        Time.timeScale = 1f;
    }

    private void Update()
    {
        if (Input.GetButtonDown("Cancel"))
        {
            ChangePauseState();
            return;
        }

        if (isPaused)
            return;

        if (!isStarted && !isFinished)
        {
            if (Input.anyKeyDown)
            {
                ResumeGame();
                isStarted = true;
            }
            return;
        }

        //Update time
        if (!isFinished)
        {
            timeScore += Time.deltaTime;
            currentScore.text = string.Format("{0:0.0}", timeScore);
        }
    }    

    private void FixedUpdate()
    {
        CheckPosition();
    }

    private void CheckPosition()
    {
        if (!isFinished && player.position.y < minY)
            Reset();
    }    

    public void Finish()
    {
        isFinished = true;
        isStarted = false;

        pausePanel.SetActive(true);
        SetFinishMenu();

        //Unlock new level
        int buildIndex = SceneManager.GetActiveScene().buildIndex;
        if (PlayerPrefs.GetInt(levelUnlockKey, 1) <= buildIndex
            && SceneManager.sceneCountInBuildSettings > buildIndex + 1)
            PlayerPrefs.SetInt(levelUnlockKey, buildIndex + 1);

        //Set new highscore
        if (timeScore < PlayerPrefs.GetFloat(highscoreKey, float.MaxValue))
        {
            PlayerPrefs.SetFloat(highscoreKey, timeScore);
        }

        PlayerPrefs.Save();
    }

    private void SetFinishMenu()
    {
        finishMenu.SetActive(true);

        int buildIndex = SceneManager.GetActiveScene().buildIndex;

        if (buildIndex <= 1)
            prevLevelButton.SetActive(false);
        else prevLevelButton.SetActive(true);

        if (SceneManager.sceneCountInBuildSettings <= buildIndex + 1)
            nextLevelButton.SetActive(false);
        else nextLevelButton.SetActive(true);

        currentScore.text = "";
        highscoreText.text = "Highscore " + string.Format("{0:0.0}", PlayerPrefs.GetFloat(highscoreKey));
        currentScoreText.text = "Score " + string.Format("{0:0.0}", timeScore);
    }

    public void GoToNextLevel()
    {
        int nextSceneBuildIndex = SceneManager.GetActiveScene().buildIndex + 1;
        SceneManager.LoadScene(nextSceneBuildIndex);
    }

    public void GoToPreviousLevel()
    {
        int prevSceneBuildIndex = SceneManager.GetActiveScene().buildIndex - 1;
        SceneManager.LoadScene(prevSceneBuildIndex);
    }

    public void GoToMainMenu()
    {
        AudioManager.instance.Stop("Music1");
        SceneManager.LoadScene(0);
    }
}
