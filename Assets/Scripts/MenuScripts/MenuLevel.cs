﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuLevel : MonoBehaviour
{
    public Transform secretImage;
    public int levelIndex;

    private void Start()
    {
        if (PlayerPrefs.GetInt("LevelUnlocked", 1) < levelIndex + 1)
            gameObject.SetActive(false);
        else
        {
            bool secretUnlocked = RewardsManager.instance.secretsUnlocked[levelIndex];
            secretImage.gameObject.SetActive(secretUnlocked);
        }
    }    
}
