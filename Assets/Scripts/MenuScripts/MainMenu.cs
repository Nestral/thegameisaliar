﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public GameObject optionsMenu;
    public GameObject levelsMenu;

    private string levelUnlockKey;

    private void Awake()
    {
        levelUnlockKey = "LevelUnlocked";
        if (PlayerPrefs.GetInt(levelUnlockKey, 1) < 1)
            PlayerPrefs.SetInt(levelUnlockKey, 1);
    }

    public void PlayGame(int levelIndex)
    {
        levelIndex += 1;
        if (levelIndex > 0 && SceneManager.sceneCountInBuildSettings > levelIndex
            && PlayerPrefs.GetInt(levelUnlockKey, 1) >= levelIndex)
        {
            AudioManager.instance.Play("Music1");
            SceneManager.LoadScene(levelIndex);
        }
    }

    public void OpenLevelsMenu()
    {
        bool open = true;
        if (levelsMenu.activeInHierarchy)
            open = false;
        optionsMenu.SetActive(false);
        levelsMenu.SetActive(open);
    }

    public void OpenOptionsMenu()
    {
        bool open = true;
        if (optionsMenu.activeInHierarchy)
            open = false;
        levelsMenu.SetActive(false);
        optionsMenu.SetActive(open);        
    }

    public void QuitGame()
    {
        Application.Quit();        
    }

    public void ResetProgress()
    {
        RewardsManager.instance.ResetProgress();
        SceneManager.LoadScene(0);
    }

    public void ChangeMasterVolume(float volume)
    {
        AudioListener.volume = volume;
    }    

    public void ChangeMusicVolume(float volume)
    {
        AudioManager.instance.ChangeVolume("Music1", volume);
    }
}
