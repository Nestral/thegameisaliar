﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class FinishSecret : MonoBehaviour
{
    public Color collectedColor;
    public Color missedColor;

    private Image image;
    private int secretIndex;

    private void Start()
    {
        image = GetComponent<Image>();
        secretIndex = SceneManager.GetActiveScene().buildIndex - 1;
        CheckSecretCollect();
    }

    public void CheckSecretCollect()
    {
        if (PlayerPrefs.GetInt("Secret" + secretIndex, 0) == 1)
        {
            image.color = collectedColor;
        }
        else
        {
            image.color = missedColor;
        }
    }
}
