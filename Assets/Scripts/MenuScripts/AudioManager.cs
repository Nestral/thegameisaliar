﻿using System;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public static AudioManager instance;

    public Sound[] sounds;

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else
        {
            Destroy(gameObject);
            return;
        }

        DontDestroyOnLoad(gameObject);

        foreach (Sound sound in sounds)
        {
            sound.source = gameObject.AddComponent<AudioSource>();
            sound.source.clip = sound.clip;
            sound.source.volume = sound.volume;
            sound.source.pitch = sound.pitch;
            sound.source.loop = sound.loop;
        }
    }

    public void ChangeVolume(string soundName, float volume)
    {
        Sound soundToPlay = Array.Find(sounds, sound => sound.name == soundName);
        if (soundToPlay == null)
        {
            Debug.LogWarning("Sound: " + soundName + " not found!");
            return;
        }
        if (soundToPlay.source == null)
        {
            Debug.LogWarning("Audio Source: " + soundName + " is null!");
            return;
        }
        soundToPlay.volume = volume;
        soundToPlay.source.volume = volume;
    }

    public void Play(string soundName)
    {
        Sound soundToPlay = Array.Find(sounds, sound => sound.name == soundName);
        if (soundToPlay == null)
        {
            Debug.LogWarning("Sound: " + soundName + " not found!");
            return;
        }
        if (soundToPlay.source == null)
        {
            Debug.LogWarning("Audio Source: " + soundName + " is null!");
            return;
        }
        soundToPlay.source.Play();        
    }

    public void Stop(string soundName)
    {
        Sound soundToPlay = Array.Find(sounds, sound => sound.name == soundName);
        if (soundToPlay == null)
        {
            Debug.LogWarning("Sound: " + soundName + " not found!");
            return;
        }
        if (soundToPlay.source == null)
        {
            Debug.LogWarning("Audio Source: " + soundName + " is null!");
            return;
        }
        soundToPlay.source.Stop();
    }

}
