﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ButtonSound : MonoBehaviour, IPointerEnterHandler, IPointerDownHandler, ISubmitHandler, ISelectHandler
{
    private AudioManager AudioManager { get { return AudioManager.instance; } }

    public void OnPointerEnter(PointerEventData ped)
    {
        Hover();
    }

    public void OnSelect(BaseEventData bed)
    {
        Hover();
    }

    private void Hover()
    {
        AudioManager.Play("ButtonHover");
    }

    public void OnPointerDown(PointerEventData ped)
    {
        Click();
    }

    public void OnSubmit(BaseEventData bed)
    {
        Click();
    }

    private void Click()
    {
        AudioManager.Play("ButtonClick");
    }
}
