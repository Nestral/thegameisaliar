﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RewardsManager : MonoBehaviour
{
    public static RewardsManager instance;
    public bool[] secretsUnlocked;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;

            CheckSecrets();

            DontDestroyOnLoad(gameObject);
        }
    }

    private void CheckSecrets()
    {
        secretsUnlocked = new bool[SceneManager.sceneCountInBuildSettings - 1];
        for (int i = 0; i < secretsUnlocked.Length; i++)
        {
            bool unlocked = PlayerPrefs.GetInt("Secret" + i, 0) == 1;
            secretsUnlocked[i] = unlocked;
        }
    }

    public void UnlockSecret(int secretIndex)
    {
        if (secretsUnlocked.Length > secretIndex)
        {
            secretsUnlocked[secretIndex] = true;
            PlayerPrefs.SetInt("Secret" + secretIndex, 1);
            PlayerPrefs.Save();
        }
    }

    public void ResetProgress()
    {
        PlayerPrefs.DeleteAll();
        CheckSecrets();
    }
}
