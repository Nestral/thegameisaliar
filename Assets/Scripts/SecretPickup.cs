﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SecretPickup : PickUpable
{
    public GameObject particles;

    private int secretIndex;

    private void Awake()
    {
        secretIndex = SceneManager.GetActiveScene().buildIndex - 1;
    }

    internal override void PickUp()
    {
        RewardsManager.instance.UnlockSecret(secretIndex);
        Instantiate(particles, transform.position, Quaternion.identity);
        AudioManager.instance.Play("PickUpSecret");

        FinishSecret[] secrets = FindObjectsOfType<FinishSecret>();
        foreach (FinishSecret secret in secrets)
        {
            secret.CheckSecretCollect();
        }

        Destroy(gameObject);
    }
}
