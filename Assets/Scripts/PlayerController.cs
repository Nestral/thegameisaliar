﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public ParticleSystem moveParticles;

    private SpriteRenderer sprite;
    public Color jumpColor;
    public Color noJumpColor;

    public float moveSpeed;
    public int moves;
    private int movesLeft;
    private Vector2 moveDirection;

    public Collider2D groundCollider;
    public LayerMask groundLayerMask;
    private bool isGrounded;

    private Rigidbody2D rigidb;
    [HideInInspector]
    public Level currentLevel;

    private void Awake()
    {
        rigidb = GetComponent<Rigidbody2D>();
        sprite = GetComponent<SpriteRenderer>();
        movesLeft = moves;
    }

    private void Update()
    {
        CheckInput();
    }

    private void CheckInput()
    {
        if (!currentLevel.isPaused)
            CheckMovementInput();
    }

    private void CheckMovementInput()
    {
        moveDirection = Vector2.zero;

        if (Input.GetButtonDown("Right"))
        {
            moveDirection.x++;
        }
        if (Input.GetButtonDown("Left"))
        {
            moveDirection.x--;
        }
        if (Input.GetButtonDown("Up"))
        {
            moveDirection.y++;
        }
        if (Input.GetButtonDown("Down"))
        {
            moveDirection.y--;
        }

        if (moveDirection != Vector2.zero)
            Move(moveDirection);
    }

    private void FixedUpdate()
    {
        CheckGroundCollision();
    }

    private void CheckGroundCollision()
    {
        isGrounded = groundCollider.IsTouchingLayers(groundLayerMask);
        if (isGrounded)
        {
            movesLeft = moves;
            sprite.color = jumpColor;
        }
        else if (movesLeft <= 0)
        {
            sprite.color = noJumpColor;
        }
    }

    private void Move(Vector2 direction)
    {
        if (movesLeft > 0 || isGrounded)
        {
            if (direction.magnitude > 1)
                direction.Normalize();

            float velx, vely;
            if (direction.x == 0)
                velx = rigidb.velocity.x;
            else velx = direction.x * moveSpeed;

            if (direction.y == 0)
                vely = rigidb.velocity.y;
            else vely = direction.y * moveSpeed;

            rigidb.velocity = new Vector2(velx, vely);
            movesLeft--;            

            AudioManager.instance.Play("Jump");
            Instantiate(moveParticles, transform.position, Quaternion.identity);
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Finish"))
        {
            currentLevel.Finish();
        }
    }
}
