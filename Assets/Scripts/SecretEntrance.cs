﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SecretEntrance : MonoBehaviour
{
    public GameObject[] secretObjects;
    public GameObject[] illusoryObjects;

    private void Start()
    {
        foreach (GameObject secretObject in secretObjects)
        {
            if (secretObject != null)
                secretObject.SetActive(false);
        }
        foreach (GameObject illusoryObject in illusoryObjects)
        {
            if (illusoryObject != null)
                illusoryObject.SetActive(true);
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            foreach (GameObject secretObject in secretObjects)
            {
                if (secretObject != null)
                    secretObject.SetActive(true);
            }
            foreach (GameObject illusoryObject in illusoryObjects)
            {
                if (illusoryObject != null)
                    illusoryObject.SetActive(false);
            }
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = new Color32(200, 100, 200, 100);
        Gizmos.DrawCube(transform.position, transform.localScale);
    }
}
