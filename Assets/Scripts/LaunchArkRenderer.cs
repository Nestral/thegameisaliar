﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(LineRenderer))]
public class LaunchArkRenderer : MonoBehaviour
{
    [HideInInspector]
    public float velocity;
    [HideInInspector]
    public float angle;
    public int resolution;
    public int maxResolution;
    public LayerMask collisionLayerMask;    

    private LineRenderer lineRenderer;
    private float gravity;
    private float radianAngle;

    private void Awake()
    {
        lineRenderer = GetComponent<LineRenderer>();
        gravity = Mathf.Abs(Physics2D.gravity.y);
    }

    private void OnValidate()
    {
        if (lineRenderer != null && Application.isPlaying)
        {
            Render();
        }
    }

    public void RenderArc(float _velocity, float _angle, int _resolution)
    {
        velocity = _velocity;
        angle = _angle;
        resolution = _resolution;
        Render();
    }

    public void RenderArc(float _velocity, float _angle)
    {
        velocity = _velocity;
        angle = _angle;
        Render();
    }

    private void Render()
    {
        Vector3[] arcArray = CalculateArcArray();
        lineRenderer.positionCount = arcArray.Length;
        lineRenderer.SetPositions(arcArray);
    }

    private Vector3[] CalculateArcArray()
    {
        List<Vector3> arcList = new List<Vector3>();

        radianAngle = Mathf.Deg2Rad * angle;
        float maxDistance = velocity * velocity * Mathf.Sin(2 * radianAngle) / gravity;

        Vector3 arcPoint = transform.position;
        int i = 0;

        while (i < maxResolution && !Physics2D.OverlapPoint(arcPoint, collisionLayerMask))
        {
            float t = (float)i / (float)resolution;
            arcPoint = CalculateArcPoint(t, maxDistance);
            arcList.Add(arcPoint);
            i++;
        }

        return arcList.ToArray();
    }

    private Vector3 CalculateArcPoint(float t, float maxDistance)
    {
        float x = t * maxDistance;
        float y = x * Mathf.Tan(radianAngle) - ((gravity * x * x)
            / (2 * velocity * velocity * Mathf.Cos(radianAngle) * Mathf.Cos(radianAngle)));

        return new Vector3(x, y) + transform.position;
    }
}
